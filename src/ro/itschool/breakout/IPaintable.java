package ro.itschool.breakout;

import java.awt.Graphics2D;

public interface IPaintable {
	
	public void paint(Graphics2D graphics);
}
