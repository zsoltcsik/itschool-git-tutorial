package ro.itschool.breakout;

public interface PaddleMovedListener {

	public void paddleMoved(int movedX);
}
