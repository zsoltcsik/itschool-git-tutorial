package ro.itschool.breakout;

public enum Side {
	TOP,
	BOTTOM,
	LEFT,
	RIGHT;
}
