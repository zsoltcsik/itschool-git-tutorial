package ro.itschool.breakout;

public enum WallSide {
	LEFT,
	TOP,
	RIGHT;
}
